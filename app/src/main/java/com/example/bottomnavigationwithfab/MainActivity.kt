package com.example.bottomnavigationwithfab

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bottomnavigationwithfab.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setup main theme
        setTheme(R.style.Theme_BottomNavigationWithFab)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // remove shadow from bottom navigation view
        binding.bottomNavigationView.background = null

        // setup place holder for bottom navigation
        binding.bottomNavigationView.menu.getItem(2).isEnabled = false
    }
}